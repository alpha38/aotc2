tusken_death_hunter = Creature:new {
	objectName = "@mob/creature_names:tusken_death_hunter",
	socialGroup = "tusken_raider",
	faction = "tusken_raider",
	level = 107,
	chanceHit = 3.5,
	damageMin = 645,
	damageMax = 1048,
	baseXp = 10174,
	baseHAM = 24000,
	baseHAMmax = 30000,
	armor = 2,
	resists = {60,60,40,40,0,40,0,40,-1},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER + STALKER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/tusken_raider.iff"},
	lootGroups = {
		{
			groups = {
				{group = "tusken_common", chance = 4000000},
				{group = "armor_attachments", chance = 2000000},
				{group = "clothing_attachments", chance = 2000000},
				{group = "krayt_tissue_common", chance = 1500000},
				{group = "krayt_pearls_tusken", chance = 500000}
			},
			lootChance = 10000000
		}
	},
	weapons = {"tusken_weapons"},
	conversationTemplate = "",
	attacks = merge(marksmanmaster,brawlermaster,fencermaster)
}

CreatureTemplates:addCreatureTemplate(tusken_death_hunter, "tusken_death_hunter")
